package com.boringville.test.daggerlombok;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.NoArgsConstructor;
import dagger.Component;
import dagger.Module;

public class App {
	public static void main(String[] args) {
		TestComponent component = Dagger_TestComponent.builder().build();
		Test test = component.test();
		test.bark();
	}
}

@NoArgsConstructor (onConstructor = @__ ({ @Inject }))
class Test {
	//	@Inject
	//	public Test() {}

	public void bark() {
		System.out.println("Woof!");
	}
}

@Singleton
@Component (modules = { TestModule.class })
interface TestComponent {
	public Test test();
}

@Module (injects = { Test.class })
class TestModule {}